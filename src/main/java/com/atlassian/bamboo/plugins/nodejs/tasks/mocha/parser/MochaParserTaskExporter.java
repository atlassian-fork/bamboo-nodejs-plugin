package com.atlassian.bamboo.plugins.nodejs.tasks.mocha.parser;

import com.atlassian.bamboo.plugins.testresultparser.task.exporter.TestParserTaskExporter;
import com.atlassian.bamboo.specs.api.builders.task.Task;
import com.atlassian.bamboo.specs.api.model.task.TaskProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationProblem;
import com.atlassian.bamboo.specs.builders.task.MochaParserTask;
import com.atlassian.bamboo.specs.model.task.MochaParserTaskProperties;
import com.atlassian.bamboo.specs.model.task.TestParserTaskProperties.TestType;
import com.atlassian.bamboo.task.ImmutableTaskDefinition;
import com.atlassian.bamboo.task.TaskConfigConstants;
import com.atlassian.bamboo.task.TaskContainer;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.task.export.TaskValidationContext;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MochaParserTaskExporter extends TestParserTaskExporter {
    @NotNull
    @Override
    public Map<String, String> toTaskConfiguration(@NotNull TaskContainer taskContainer, @NotNull TaskProperties taskProperties) {
        if (taskProperties instanceof MochaParserTaskProperties) {
            // importing deprecated mocha parser task properties
            final MochaParserTaskProperties typedTaskProperties = (MochaParserTaskProperties) taskProperties;
            return ImmutableMap.<String, String>builder()
                    .put(MochaParserConfigurator.PATTERN, typedTaskProperties.getTestFilePattern())
                    .put(TaskConfigConstants.CFG_WORKING_SUBDIRECTORY, StringUtils.defaultString(typedTaskProperties.getWorkingSubdirectory()))
                    .put(TaskConfigConstants.CFG_TEST_OUTDATED_RESULTS_FILE, Boolean.toString(typedTaskProperties.isPickUpTestResultsCreatedOutsideOfThisBuild()))
                    .build();
        } else {
            // importing generic parser task properties
            return transformKey(super.toTaskConfiguration(taskContainer, taskProperties),
                    TaskConfigConstants.CFG_TEST_RESULTS_FILE_PATTERN,
                    MochaParserConfigurator.PATTERN);
        }
    }

    @NotNull
    @Override
    protected TestType getTestType() {
        return TestType.MOCHA;
    }

    @NotNull
    @Override
    public Task toSpecsEntity(@NotNull TaskDefinition taskDefinition) {
        final Map<String, String> taskConfiguration = taskDefinition.getConfiguration();
        final String workingSubDirectory = taskConfiguration.getOrDefault(TaskConfigConstants.CFG_WORKING_SUBDIRECTORY, null);

        if (StringUtils.isNotBlank(workingSubDirectory)) {
            // exporting as deprecated mocha parser task due to legacy configuration field
            final MochaParserTask mochaParserTask = new MochaParserTask()
                    .testFilePattern(taskConfiguration.get(MochaParserConfigurator.PATTERN))
                    .workingSubdirectory(workingSubDirectory);
            if (taskConfiguration.containsKey(TaskConfigConstants.CFG_TEST_OUTDATED_RESULTS_FILE)) {
                mochaParserTask.pickUpTestResultsCreatedOutsideOfThisBuild(
                        Boolean.parseBoolean(taskConfiguration.get(TaskConfigConstants.CFG_TEST_OUTDATED_RESULTS_FILE)));
            }
            return mochaParserTask;
        } else {
            // exporting as generic parser task
            return super.toSpecsEntity(new MochaTaskDefinition(taskDefinition));
        }
    }

    @NotNull
    @Override
    public List<ValidationProblem> validate(@NotNull TaskValidationContext taskValidationContext, @NotNull TaskProperties taskProperties) {
        if (taskProperties instanceof MochaParserTaskProperties) {
            // validating deprecated mocha parser task properties
            final List<ValidationProblem> validationProblems = new ArrayList<>();
            final MochaParserTaskProperties typedTaskProperties = (MochaParserTaskProperties) taskProperties;
            if (StringUtils.isBlank(typedTaskProperties.getTestFilePattern())) {
                validationProblems.add(new ValidationProblem(MochaParserTaskProperties.VALIDATION_CONTEXT, "Test file pattern is not defined"));
            }
            return validationProblems;
        } else {
            // validating generic parser task properties
            return super.validate(taskValidationContext, taskProperties);
        }
    }

    /**
     * Transforms a single entry in a map, moving the value under the given key to a new key. If the map does not
     * contain an entry under the given key, no actions will be performed.
     *
     * @param config     original map
     * @param currentKey key to transform
     * @param newKey     new key under which value will be put
     * @return a new, immutable map with transformed entry
     */
    @NotNull
    private static Map<String, String> transformKey(@NotNull Map<String, String> config, @NotNull String currentKey, @NotNull String newKey) {
        final Map<String, String> translatedConfig = new HashMap<>(config);
        if (config.containsKey(currentKey)) {
            translatedConfig.put(newKey, config.get(currentKey));
            translatedConfig.remove(currentKey);
        }
        return Collections.unmodifiableMap(translatedConfig);
    }

    /**
     * A decorator for {@link TaskDefinition} to translate configuration properties for generic
     * {@link TestParserTaskExporter}.
     */
    private static class MochaTaskDefinition extends ImmutableTaskDefinition {
        MochaTaskDefinition(@NotNull TaskDefinition taskDefinition) {
            super(taskDefinition);
        }

        @NotNull
        @Override
        public Map<String, String> getConfiguration() {
            return transformKey(super.getConfiguration(),
                    MochaParserConfigurator.PATTERN,
                    TaskConfigConstants.CFG_TEST_RESULTS_FILE_PATTERN);
        }
    }
}

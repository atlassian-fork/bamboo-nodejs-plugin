package com.atlassian.bamboo.plugins.nodejs.tasks.gulp;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.plugins.nodejs.tasks.AbstractNodeRequiringTaskConfigurator;
import com.atlassian.bamboo.specs.builders.task.GulpTask;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import java.util.Map;
import java.util.Set;

public class GulpConfigurator extends AbstractNodeRequiringTaskConfigurator {
    public static final String GULP_DEFAULT_EXECUTABLE = GulpTask.DEFAULT_GULP_EXECUTABLE;

    public static final String GULP_RUNTIME = "gulpRuntime";
    public static final String TASK = "task";
    public static final String CONFIG_FILE = "configFile";

    protected static final Set<String> FIELDS_TO_COPY = ImmutableSet.<String>builder()
            .addAll(AbstractNodeRequiringTaskConfigurator.FIELDS_TO_COPY)
            .add(GULP_RUNTIME)
            .add(TASK)
            .add(CONFIG_FILE)
            .build();
    protected static final Map<String, Object> DEFAULT_FIELD_VALUES = ImmutableMap.<String, Object>builder()
            .putAll(AbstractNodeRequiringTaskConfigurator.DEFAULT_FIELD_VALUES)
            .put(GULP_RUNTIME, GULP_DEFAULT_EXECUTABLE)
            .build();

    @Override
    public void validate(@NotNull ActionParametersMap params, @NotNull ErrorCollection errorCollection) {
        super.validate(params, errorCollection);
        if (StringUtils.isBlank(params.getString(GULP_RUNTIME))) {
            errorCollection.addError(GULP_RUNTIME, i18nResolver.getText("gulp.runtime.error.empty"));
        }
    }

    @NotNull
    @Override
    public Set<String> getFieldsToCopy() {
        return FIELDS_TO_COPY;
    }

    @NotNull
    @Override
    public Map<String, Object> getDefaultFieldValues() {
        return DEFAULT_FIELD_VALUES;
    }
}

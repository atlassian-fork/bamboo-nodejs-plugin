package com.atlassian.bamboo.plugins.nodejs.tasks.npm;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.plugins.nodejs.tasks.AbstractNodeRequiringTaskConfigurator;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.google.common.collect.ImmutableSet;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import java.util.Set;

public class NpmConfigurator extends AbstractNodeRequiringTaskConfigurator {
    public static final String COMMAND = "command";
    public static final String ISOLATED_CACHE = "isolatedCache";

    protected static final Set<String> FIELDS_TO_COPY = ImmutableSet.<String>builder()
            .addAll(AbstractNodeRequiringTaskConfigurator.FIELDS_TO_COPY)
            .add(COMMAND)
            .add(ISOLATED_CACHE)
            .build();

    @Override
    public void validate(@NotNull ActionParametersMap params, @NotNull ErrorCollection errorCollection) {
        super.validate(params, errorCollection);
        if (StringUtils.isEmpty(params.getString(COMMAND))) {
            errorCollection.addError(COMMAND, i18nResolver.getText("npm.command.error.empty"));
        }
    }

    @NotNull
    @Override
    public Set<String> getFieldsToCopy() {
        return FIELDS_TO_COPY;
    }
}

package com.atlassian.bamboo.plugins.nodejs.tasks.mocha.runner;

import com.atlassian.bamboo.plugins.nodejs.tasks.AbstractNodeRequiringTaskExporter;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import com.atlassian.bamboo.specs.api.validators.common.ValidationProblem;
import com.atlassian.bamboo.specs.builders.task.MochaRunnerTask;
import com.atlassian.bamboo.specs.model.task.MochaRunnerTaskProperties;
import com.atlassian.bamboo.ww2.actions.build.admin.create.UIConfigSupport;
import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MochaRunnerTaskExporter extends AbstractNodeRequiringTaskExporter<MochaRunnerTaskProperties, MochaRunnerTask> {
    @Autowired
    public MochaRunnerTaskExporter(UIConfigSupport uiConfigSupport) {
        super(MochaRunnerTaskProperties.class, uiConfigSupport);
    }

    @NotNull
    @Override
    protected ValidationContext getValidationContext() {
        return MochaRunnerTaskProperties.VALIDATION_CONTEXT;
    }

    @NotNull
    @Override
    public Map<String, String> toTaskConfiguration(@NotNull MochaRunnerTaskProperties taskProperties) {
        final Map<String, String> config = new HashMap<>();
        config.put(MochaRunnerConfigurator.MOCHA_RUNTIME, taskProperties.getMochaExecutable());
        config.put(MochaRunnerConfigurator.TEST_FILES, taskProperties.getTestFilesAndDirectories());
        config.put(MochaRunnerConfigurator.PARSE_TEST_RESULTS, Boolean.toString(taskProperties.isParseTestResults()));
        config.put(MochaRunnerConfigurator.ARGUMENTS, taskProperties.getArguments());
        return config;
    }

    @NotNull
    @Override
    protected MochaRunnerTask toSpecsEntity(@NotNull Map<String, String> taskConfiguration) {
        return new MochaRunnerTask()
                .mochaExecutable(taskConfiguration.get(MochaRunnerConfigurator.MOCHA_RUNTIME))
                .testFilesAndDirectories(taskConfiguration.get(MochaRunnerConfigurator.TEST_FILES))
                .parseTestResults(Boolean.parseBoolean(taskConfiguration.getOrDefault(MochaRunnerConfigurator.PARSE_TEST_RESULTS,
                        Boolean.toString(MochaRunnerTask.DEFAULT_PARSE_TEST_RESULTS))))
                .arguments(taskConfiguration.getOrDefault(MochaRunnerConfigurator.ARGUMENTS, null));
    }

    @NotNull
    @Override
    public List<ValidationProblem> validate(@NotNull MochaRunnerTaskProperties taskProperties) {
        final List<ValidationProblem> validationProblems = new ArrayList<>();
        if (StringUtils.isBlank(taskProperties.getMochaExecutable())) {
            validationProblems.add(new ValidationProblem(getValidationContext(), "Mocha executable is not defined"));
        }
        if (StringUtils.isBlank(taskProperties.getTestFilesAndDirectories())) {
            validationProblems.add(new ValidationProblem(getValidationContext(), "Test files and/or directories are not defined"));
        }
        return validationProblems;
    }
}

package com.atlassian.bamboo.plugins.nodejs.tasks.npm;

import com.atlassian.bamboo.build.CustomBuildProcessor;
import com.atlassian.bamboo.v2.build.BuildContext;
import com.atlassian.bamboo.v2.build.agent.ExecutableBuildAgent;
import com.atlassian.bamboo.v2.build.agent.capability.AgentContext;
import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

import static com.google.common.base.Preconditions.checkState;

/**
 * A custom build processor that cleans up the temporary cache directory used by npm tasks.
 */
public class NpmCacheCleanupProcessor implements CustomBuildProcessor {
    private BuildContext buildContext;

    @Inject
    private AgentContext agentContext;

    @Override
    public void init(@NotNull BuildContext buildContext) {
        this.buildContext = buildContext;
    }

    @NotNull
    @Override
    public BuildContext call() throws Exception {
        final ExecutableBuildAgent buildAgent = agentContext.getBuildAgent();
        checkState(buildAgent != null, "Agent should be always set within build pipeline");

        NpmIsolatedCacheHelper.deleteTemporaryCacheDirectory(buildAgent.getId());

        return buildContext;
    }

    //backward compatibility for platform pre-5.0
    public void setAgentContext(AgentContext agentContext) {
        this.agentContext = agentContext;
    }
}

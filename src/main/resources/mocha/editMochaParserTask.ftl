[@s.textfield labelKey='mocha.parser.testPattern' name='testPattern' required=true /]

[@ui.bambooSection titleKey='repository.advanced.option' collapsible=true isCollapsed=!(workingSubDirectory?has_content)]
    [@s.textfield labelKey='builder.common.sub' name='workingSubDirectory' cssClass="long-field" /]
    [@s.checkbox labelKey="builder.common.tests.pickup.outdated.files" name="pickupOutdatedFiles"/]
[/@ui.bambooSection]


package it.com.atlassian.bamboo.plugins.nodejs.pageobjects;

import com.atlassian.bamboo.pageobjects.elements.TextElement;
import com.atlassian.bamboo.pageobjects.pages.tasks.TaskComponent;
import com.atlassian.bamboo.plugins.nodejs.tasks.node.NodeConfigurator;
import com.atlassian.bamboo.task.TaskConfigConstants;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.Options;
import com.atlassian.pageobjects.elements.SelectElement;

import java.util.Map;

public class NodeTaskComponent extends ComponentWithAdvancedOptions implements TaskComponent {
    public static final String TASK_NAME = "Node.js";

    @ElementBy(name = NodeConfigurator.NODE_RUNTIME)
    private SelectElement nodeRuntimeField;

    @ElementBy(name = NodeConfigurator.COMMAND)
    private TextElement commandField;

    @ElementBy(name = NodeConfigurator.ARGUMENTS)
    private TextElement argumentsField;

    @ElementBy(name = TaskConfigConstants.CFG_WORKING_SUB_DIRECTORY)
    private TextElement workingSubDirectoryField;

    @ElementBy(name = TaskConfigConstants.CFG_ENVIRONMENT_VARIABLES)
    private TextElement environmentVariablesField;

    @Override
    public void updateTaskDetails(Map<String, String> config) {
        this.withAdvancedOptions();

        if (config.containsKey(NodeConfigurator.NODE_RUNTIME)) {
            nodeRuntimeField.select(Options.value(config.get(NodeConfigurator.NODE_RUNTIME)));
        }
        if (config.containsKey(NodeConfigurator.COMMAND)) {
            commandField.setText(config.get(NodeConfigurator.COMMAND));
        }
        if (config.containsKey(NodeConfigurator.ARGUMENTS)) {
            argumentsField.setText(config.get(NodeConfigurator.ARGUMENTS));
        }
        if (config.containsKey(TaskConfigConstants.CFG_WORKING_SUB_DIRECTORY)) {
            workingSubDirectoryField.setText(config.get(TaskConfigConstants.CFG_WORKING_SUB_DIRECTORY));
        }
        if (config.containsKey(TaskConfigConstants.CFG_ENVIRONMENT_VARIABLES)) {
            environmentVariablesField.setText(config.get(TaskConfigConstants.CFG_ENVIRONMENT_VARIABLES));
        }
    }
}
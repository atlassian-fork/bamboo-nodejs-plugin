package com.atlassian.bamboo.plugins.nodejs.tasks.mocha.parser;

import com.atlassian.bamboo.specs.api.validators.common.ValidationProblem;
import com.atlassian.bamboo.specs.builders.task.MochaParserTask;
import com.atlassian.bamboo.specs.builders.task.TestParserTask;
import com.atlassian.bamboo.specs.model.task.MochaParserTaskProperties;
import com.atlassian.bamboo.specs.model.task.TestParserTaskProperties;
import com.atlassian.bamboo.task.TaskConfigConstants;
import com.atlassian.bamboo.task.TaskContainer;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.task.TaskDefinitionImpl;
import com.atlassian.bamboo.task.export.TaskValidationContext;
import com.google.common.collect.ImmutableMap;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.List;
import java.util.Map;

import static com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders.build;
import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.mockito.Mockito.mock;

@RunWith(JUnitParamsRunner.class)
public class MochaParserTaskExporterTest {
    @Rule
    public final MockitoRule mockitoRule = MockitoJUnit.rule();

    @InjectMocks
    private MochaParserTaskExporter mochaParserTaskExporter;

    @Test
    public void testDeprecatedToTaskConfiguration() {
        // provided
        final String testFilePattern = "mocha-results.json";
        final String workingSubdirectory = "plugin";
        final boolean pickUpTestResultsCreatedOutsideOfThisBuild = true;

        final TaskContainer taskContainer = mock(TaskContainer.class);

        final MochaParserTask mochaParserTask = new MochaParserTask()
                .testFilePattern(testFilePattern)
                .workingSubdirectory(workingSubdirectory)
                .pickUpTestResultsCreatedOutsideOfThisBuild(pickUpTestResultsCreatedOutsideOfThisBuild);
        final MochaParserTaskProperties mochaParserTaskProperties = build(mochaParserTask);

        // when
        final Map<String, String> configuration = mochaParserTaskExporter.toTaskConfiguration(taskContainer, mochaParserTaskProperties);

        // then
        assertThat(configuration.get(MochaParserConfigurator.PATTERN), is(testFilePattern));
        assertThat(configuration.get(TaskConfigConstants.CFG_WORKING_SUBDIRECTORY), is(workingSubdirectory));
        assertThat(configuration.get(TaskConfigConstants.CFG_TEST_OUTDATED_RESULTS_FILE), is(Boolean.toString(pickUpTestResultsCreatedOutsideOfThisBuild)));
    }

    @Test
    public void testCurrentToTaskConfiguration() {
        // provided
        final String testFilePattern = "mocha-results.json";
        final boolean pickUpTestResultsCreatedOutsideOfThisBuild = true;

        final TaskContainer taskContainer = mock(TaskContainer.class);

        final TestParserTask testParserTask = TestParserTask.createMochaParserTask()
                .resultDirectories(testFilePattern)
                .pickUpTestResultsCreatedOutsideOfThisBuild(pickUpTestResultsCreatedOutsideOfThisBuild);
        final TestParserTaskProperties testParserTaskProperties = build(testParserTask);

        // when
        final Map<String, String> configuration = mochaParserTaskExporter.toTaskConfiguration(taskContainer, testParserTaskProperties);

        // then
        assertThat(configuration.get(MochaParserConfigurator.PATTERN), is(testFilePattern));
        assertThat(configuration.get(TaskConfigConstants.CFG_WORKING_SUBDIRECTORY), is(nullValue()));
        assertThat(configuration.get(TaskConfigConstants.CFG_TEST_OUTDATED_RESULTS_FILE), is(Boolean.toString(pickUpTestResultsCreatedOutsideOfThisBuild)));
    }

    @Test
    public void testDeprecatedToSpecsEntity() {
        // provided
        final String testFilePattern = "mocha-results.json";
        final String workingSubdirectory = "plugin";
        final boolean outdatedResults = true;

        final Map<String, String> configuration = ImmutableMap.<String, String>builder()
                .put(MochaParserConfigurator.PATTERN, testFilePattern)
                .put(TaskConfigConstants.CFG_WORKING_SUBDIRECTORY, workingSubdirectory)
                .put(TaskConfigConstants.CFG_TEST_OUTDATED_RESULTS_FILE, Boolean.toString(outdatedResults))
                .build();
        final TaskDefinition taskDefinition = new TaskDefinitionImpl(100L, "pluginKey", null, true, configuration, false);

        // when
        final MochaParserTaskProperties taskProperties = (MochaParserTaskProperties) build(mochaParserTaskExporter.toSpecsEntity(taskDefinition));

        // then
        assertThat(taskProperties.getTestFilePattern(), is(testFilePattern));
        assertThat(taskProperties.getWorkingSubdirectory(), is(workingSubdirectory));
        assertThat(taskProperties.isPickUpTestResultsCreatedOutsideOfThisBuild(), is(outdatedResults));
    }

    @Test
    @Parameters
    public void testCurrentToSpecsEntity(String workingSubdirectory) {
        // provided
        final String testFilePattern = "**/plugin1/mocha.json, **/plugin2/mocha.json";
        final boolean outdatedResults = true;

        final ImmutableMap.Builder<String, String> configurationBuilder = ImmutableMap.<String, String>builder()
                .put(MochaParserConfigurator.PATTERN, testFilePattern)
                .put(TaskConfigConstants.CFG_TEST_OUTDATED_RESULTS_FILE, Boolean.toString(outdatedResults));
        if (workingSubdirectory != null) {
            configurationBuilder.put(TaskConfigConstants.CFG_WORKING_SUBDIRECTORY, workingSubdirectory);
        }
        final Map<String, String> configuration = configurationBuilder.build();
        final TaskDefinition taskDefinition = new TaskDefinitionImpl(100L, "pluginKey", null, true, configuration, false);

        // when
        final TestParserTaskProperties taskProperties = (TestParserTaskProperties) build(mochaParserTaskExporter.toSpecsEntity(taskDefinition));

        // then
        assertThat(taskProperties.getResultDirectories(), is(asList("**/plugin1/mocha.json", "**/plugin2/mocha.json")));
        assertThat(taskProperties.getPickUpTestResultsCreatedOutsideOfThisBuild(), is(outdatedResults));
    }

    private Object[] parametersForTestCurrentToSpecsEntity() {
        return new Object[]{
                new Object[]{null},
                new Object[]{""},
                new Object[]{" "}};
    }

    @Test
    public void testDeprecatedValidation() {
        // provided
        final TaskValidationContext taskValidationContext = mock(TaskValidationContext.class);

        final MochaParserTask mochaParserTask = new MochaParserTask();
        final MochaParserTaskProperties mochaParserTaskProperties = build(mochaParserTask);

        // when
        final List<ValidationProblem> validationProblems = mochaParserTaskExporter.validate(taskValidationContext, mochaParserTaskProperties);

        // then
        assertThat(validationProblems, is(empty()));
    }

    @Test
    public void testCurrentValidation() {
        // provided
        final TaskValidationContext taskValidationContext = mock(TaskValidationContext.class);

        final TestParserTask testParserTask = TestParserTask.createMochaParserTask().defaultResultDirectory();
        final TestParserTaskProperties mochaParserTaskProperties = build(testParserTask);

        // when
        final List<ValidationProblem> validationProblems = mochaParserTaskExporter.validate(taskValidationContext, mochaParserTaskProperties);

        // then
        assertThat(validationProblems, is(empty()));
    }
}
